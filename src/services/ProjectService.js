export default {
    getById: function (id) {
        let projectFound = null;
        this.getData().forEach(function (project) {
            if (project.id === id) {
                projectFound = project;
            }
        });
        return projectFound;
    },
    getData: function() {
        return [
            {
                id: 42,
                title: 'BJB'
            },
            {
                id: 43,
                title: 'JBJ'
            },
        ];
    },
};
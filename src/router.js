import Vue from 'vue';
import Home from "@/components/Home";
import Project from "@/components/Project";
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home',
            meta: { title: 'Home' },
        },
        {
            path: '/project/:id',
            component: Project,
            name: 'project',
            meta: { title: 'Project' },
            props: true,
        },
    ],
    mode: 'history'
});
